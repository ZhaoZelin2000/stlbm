// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

// The file cavity.cpp implements a flow in a lid-driven cavity.
// To run it, you must place a configuration file names "config" in the current
// directory. An example configuration file should be available in the source
// directory of cavity.cpp.
//
// This is the first generic application that was written for the stlbm
// library. It runs with all six data structures (twopop-aos, twopop-soa,
// swap-aos, swap-soa, aa-aos, aa-soa) and all nine collision models of stlbm.

#include "lbm_q27.h"

#include "twopop_aos_q27.h"
#include "twopop_aos_bgk_unrolled_q27.h"
#include "twopop_aos_trt_unrolled_q27.h"

#include "twopop_soa_q27.h"
#include "twopop_soa_bgk_unrolled_q27.h"
#include "twopop_soa_trt_unrolled_q27.h"
#include "twopop_soa_bgk_o4_q27.h"
#include "twopop_soa_rm_q27.h"
#include "twopop_soa_hm_q27.h"
#include "twopop_soa_cm_q27.h"
#include "twopop_soa_chm_q27.h"
#include "twopop_soa_k_q27.h"
#include "twopop_soa_rr_q27.h"

#include "swap_aos_q27.h"
#include "swap_aos_bgk_unrolled_q27.h"
#include "swap_aos_trt_unrolled_q27.h"

#include "swap_soa_q27.h"
#include "swap_soa_bgk_unrolled_q27.h"
#include "swap_soa_trt_unrolled_q27.h"

#include "aa_aos_q27.h"
#include "aa_aos_bgk_unrolled_q27.h"
#include "aa_aos_trt_unrolled_q27.h"

#include "aa_soa_q27.h"
#include "aa_soa_bgk_unrolled_q27.h"
#include "aa_soa_trt_unrolled_q27.h"
#include "aa_soa_bgk_o4_q27.h"
#include "aa_soa_rm_q27.h"
#include "aa_soa_hm_q27.h"
#include "aa_soa_cm_q27.h"
#include "aa_soa_chm_q27.h"
#include "aa_soa_k_q27.h"
#include "aa_soa_rr_q27.h"

#include "utility_q27.h"
#include <string>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <execution>
#include <chrono>
#include <cassert>

using namespace std;
using namespace std::chrono;

int out_freq = 0;       // Non-benchmark mode: Frequency in LU of output message (use 0 for no messages)
int vtk_freq = 0;       // Non-benchmark mode: Frequency in LU of VTK data output (use 0 for no VTK)
int data_freq = 0;      // Non-benchmark mode: Frequency in LU of full data dump (use 0 for no data dump)
double start_avg_profiles = 0.; // Non-benchmark mode: Time (dim.less units) after which profiles are averaged, not instantaneous
int bench_ini_iter = 0; // Benchmark mode: Number of warmup iterations
int bench_max_iter = 0; // Benchmark mode: Total number of iterations

// Initializes the flag matrix and the populations to set up a cavity flow. In particular, the
// momentum-exchange term is assigned to the wall cells.
template<class LBM>
void iniCavity(LBM& lbm, double ulb, vector<double> const& ulid) {
    Dim const& dim = lbm.dim;
    for (size_t i = 0; i < dim.nelem; ++i) {
        auto[iX, iY, iZ] = lbm.i_to_xyz(i);
        if (iX == 0 || iX == dim.nx-1 || iY == 0 || iY == dim.ny-1 || iZ == 0 || iZ == dim.nz-1) {
            lbm.flag[i] = CellType::bounce_back;
            if (iX == dim.nx-1) {
                for (int k = 0; k < 27; ++k) {
                    lbm.f(i, k) = - 6. * lbm.t[k] * ulb * (
                        lbm.c[k][0] * ulid[0] + lbm.c[k][1] * ulid[1] + lbm.c[k][2] * ulid[2] );
                }
            }
            else {
                for (int k = 0; k < 27; ++k) {
                    lbm.f(i, k) = 0.;
                }
            }
        }
        else {
            lbm.flag[i] = CellType::bulk;
        }
    }
}

// Compute lattice-unit variables and discrete space and time step for a given lattice
// velocity (acoustic scaling).
auto lbParameters(double ulb, int lref, double Re) {
    double nu = ulb * static_cast<double>(lref) / Re;
    double omega = 1. / (3. * nu + 0.5);
    double dx = 1. / static_cast<double>(lref);
    double dt = dx * ulb;
    return make_tuple(nu, omega, dx, dt);
}

// Print the simulation parameters to the terminal.
void printParameters(bool benchmark, double Re, double omega, double ulb, Dim dim, double max_t) {
    if (benchmark) {
        cout << "Lid-driven 3D cavity, benchmark mode" << endl;
    }
    else {
        cout << "Lid-driven 3D cavity, production mode" << endl;
    }
    cout << "Size = {" << dim.nx << ", " << dim.ny << ", " << dim.nz << "}" << endl;
    cout << "Re = " << Re << endl;
    cout << "omega = " << omega << endl;
    cout << "ulb = " << ulb << endl;
    if (benchmark) {
        cout << "Now running " << bench_ini_iter << " warm-up iterations." << endl;
    }
    else {
        cout << "max_t = " << max_t << endl;
    }
}

// Return a new clock for the current time, for benchmarking.
auto restartClock() {
    return make_pair(high_resolution_clock::now(), 0);
}

// Compute the time elapsed since a starting point, and the corresponding
// performance of the code in Mega Lattice site updates per second (MLups).
template<class TimePoint>
double printMlups(TimePoint start, int clock_iter, size_t nelem) {
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    double mlups = static_cast<double>(nelem * clock_iter) / duration.count();

    cout << "Benchmark result: " << setprecision(4) << mlups << " MLUPS" << endl;
    cout << endl;
    return mlups;
}

// Run a regression test for a specific pre-recorded value of the average energy.
void runRegression(double energy, int time_iter, LBModel model) {
    double reference_energy = 2.0824506486;//2.09868507623;
    if (model==LBModel::trt) reference_energy = 2.02204344737; //2.01952035423;
    else if (model==LBModel::bgk_o4 || model==LBModel::rm || model==LBModel::hm ||
             model==LBModel::cm || model==LBModel::chm) reference_energy = 2.08244218633; //2.09867586186;
    else if (model==LBModel::k) reference_energy = 2.08243354542; //2.09866693596;
    else if (model==LBModel::rr) reference_energy = 2.0781490171; //2.08476112669;
    cout << "Regression test at iteration " << time_iter << ": Average energy = "
         << setprecision(12) << energy;
    if (std::fabs(energy - reference_energy) < 1.e-11) {
        cout << ": OK" << endl;
    }
    else {
        cout << ": FAILED" << endl;
        cout << "Expected the value " << reference_energy << endl;
        throw runtime_error("Regression test failed.");
    }
}


// Runs a simulation of a flow in a lid-driven cavity using the two-population scheme.
// The function is templated to allow both an instantiation with the array-of-structure and
// the structure-of-array layout, and potentially an instantiation of unrolled or non-unrolled
// code.
template<class LBM>
double runCavityTwoPop(bool benchmark, double Re, double ulb, int N, double yratio, double zratio, double max_t, LBModel model)
{
    // CellData is either a double (structure-of-array) or an array<double, 27> (array-of-structure).
    using CellData = typename LBM::CellData; 
    vector<double> ulid {0., 1., 0.}; // Velocity on top lid in dimensionless units.

    int nx = N;
    int ny = 2 + (int) round( (double)(N - 2) * yratio );
    int nz = 2 + (int) round( (double)(N - 2) * zratio );
    Dim dim {nx, ny, nz};

    auto[nu, omega, dx, dt] = lbParameters(ulb, ny - 2, Re);
    printParameters(benchmark, Re, omega, ulb, dim, max_t);

    vector<CellData> lattice_vect(LBM::sizeOfLattice(dim.nelem));
    CellData* lattice = &lattice_vect[0];

    // The "vector" is used as a convenient way to allocate the flag array on the heap.
    vector<CellType> flag_vect(dim.nelem);
    CellType* flag = &flag_vect[0];

    // The "vector" is used as a convenient way to allocate the integer value "parity"
    // on the heap. That's a necessity when the code is used on GPUs, because the
    // parity (which is 0/1, depending on whether the current time iteration is even/odd)
    // is modified on the host, and accessed on the device. The parity flag is used
    // to decide in which of the two population arrays the pre-collision LB variables
    // are located.
    vector<int> parity_vect {0};
    int* parity = &parity_vect[0];

    // The lattice constants (discrete velocities, opposite indices, weghts) are mostly
    // used in the not-unrolled versions of the code. They must be allocated on the
    // heap so they can be shared with the device in case of a GPU execution.
    auto[c, opp, t] = d3q27_constants();
    // Instantiate the function object for the for_each call.
    bool periodic = false;
    LBM lbm{lattice, flag, parity, &c[0], &opp[0], &t[0], omega, dim, model, periodic};

    // Initialize the populations.
    for_each(lattice, lattice + dim.nelem, [&lbm](CellData& f0) { lbm.iniLattice(f0); });

    // Set up the geometry of the cavity.
    iniCavity(lbm, ulb, ulid);
    // Reset the clock, to be used when a benchmark simulation is executed.
    auto[start, clock_iter] = restartClock();

    // The average energy, dependent on time, can be used to monitor convergence, or statistical
    // convergence, of the simulation.
    ofstream energyfile("energy.dat");
    // Storage space for the time-averaged profiles, if such profiles are computed.
    vector<array<double,3>> xprof(dim.nx-2, {0., 0., 0.}), yprof(dim.ny-2, {0., 0., 0.}), zprof(dim.nz-2, {0., 0., 0.});
    int num_profiles = 0; // Number of time iterations taken into account to compute time-averaged profiles.
    // Maximum number of time iterations depending on whether the simulation is in benchmark mode or production mode.
    int max_time_iter = benchmark ? bench_max_iter : static_cast<int>(max_t / dt);
    for (int time_iter = 0; time_iter < max_time_iter; ++time_iter) {
        if (benchmark && time_iter == bench_ini_iter)  {
            cout << "Now running " << bench_max_iter - bench_ini_iter
                 << " benchmark iterations." << endl;
            tie(start, clock_iter) = restartClock();
        }
        // The regression case has been registered at a resolution of N=102 after 150 iterations.
        // This test should be executed at every code change.
        if (N == 102 && time_iter == 150) {
            runRegression(computeEnergy(lbm), time_iter, model);
        }
        // If this option is chosen in the config file, save the full domain in VTK files periodically.
        if (!benchmark && vtk_freq != 0 && time_iter % vtk_freq == 0 && time_iter > 0) {
            saveVtkFields(lbm, time_iter);
        }
        // If this option is chosen in the config file, save the raw populations in text files periodically.
        if (!benchmark && data_freq != 0 && time_iter % data_freq == 0 && time_iter > 0) {
            saveFields(lbm);
        }
        // If this option is chosen in the config file, output some statistics and write the center-line
        // velocity profiles to a text file.
        if (!benchmark && out_freq != 0 && time_iter % out_freq == 0 && time_iter > 0) {
            cout << "Saving profiles at iteration " << time_iter
                 << ", t = " << setprecision(4) << time_iter * dt << setprecision(3) << " [" << time_iter * dt / max_t * 100. << "%]" << endl;
            bool writeProfileToDisk = ( time_iter % (20*out_freq) == 0 );
            if (start_avg_profiles != 0. && time_iter * dt > start_avg_profiles) {
                ++num_profiles;
                saveProfiles(lbm, ulb, xprof, yprof, zprof, num_profiles, writeProfileToDisk);
            }
            else {
                if (writeProfileToDisk) {
                    saveProfiles(lbm, ulb);
                }
            }
            double energy = computeEnergy(lbm) *dx*dx / (dt*dt);
            cout << "Average energy: " << setprecision(8) << energy << endl;
            energyfile << setw(10) << time_iter * dt << setw(16) << setprecision(8) << energy << "\n";
        }

        // With the double-population scheme, collision and streaming are fused, and are executed in the
        // following loop.
        for_each(execution::par_unseq, lattice, lattice + dim.nelem, lbm);
        // After a collision-streaming cycle, swap the parity for the next iteration.
        *parity = 1 - *parity;
        ++clock_iter;
    }

    if (benchmark) {
        return printMlups(start, clock_iter, dim.nelem);
    }
    else {
        return 0.;
    }
}

// Runs a simulation of a flow in a lid-driven cavity using the swap scheme.
// The function is templated to allow both an instantiation with the array-of-structure and
// the structure-of-array layout, and potentially an instantiation of unrolled or non-unrolled
// code.
template<class Collide, class Stream>
double runCavitySwap(bool benchmark, double Re, double ulb, int N, double yratio, double zratio, double max_t, LBModel model)
{
    // CellData is either a double (structure-of-array) or an array<double, 27> (array-of-structure).
    using CellData = typename Collide::CellData;
    vector<double> ulid {0., 1., 0.}; // Velocity on top lid in dimensionless units.

    int nx = N;
    int ny = 2 + (int) round( (double)(N - 2) * yratio );
    int nz = 2 + (int) round( (double)(N - 2) * zratio );
    Dim dim {nx, ny, nz};

    auto[nu, omega, dx, dt] = lbParameters(ulb, ny - 2, Re);
    printParameters(benchmark, Re, omega, ulb, dim, max_t);

    vector<CellData> lattice_vect(Collide::sizeOfLattice(dim.nelem));
    CellData* lattice = &lattice_vect[0];

    // We use the "vector" as a convenient way to allocate the flag array on the heap.
    vector<CellType> flag_vect(dim.nelem);
    CellType* flag = &flag_vect[0];

    // The lattice constants (discrete velocities, opposite indices, weghts) are mostly
    // used in the not-unrolled versions of the code. They must be allocated on the
    // heap so they can be shared with the device in case of a GPU execution.
    auto[c, opp, t] = d3q27_constants();
    // Instantiate two function objects for the respective two for_each calls, as collision
    // and streaming are not fused with the swap scheme.
    bool periodic = false;
    Collide collide{{lattice, flag, &c[0], &opp[0], &t[0], omega, dim, model, periodic}};
    Stream stream{{lattice, flag, &c[0], &opp[0], &t[0], omega, dim, model, periodic}};

    // Initialize the populations.
    for_each(lattice, lattice + dim.nelem, [&collide](CellData& f0) { collide.iniLattice(f0); });

    // Set up the geometry of the cavity.
    iniCavity(collide, ulb, ulid);
    // Reset the clock, to be used when a benchmark simulation is executed.
    auto[start, clock_iter] = restartClock();

    // The average energy, dependent on time, can be used to monitor convergence, or statistical
    // convergence, of the simulation.
    ofstream energyfile("energy.dat");
    // Storage space for the time-averaged profiles, if such profiles are computed.
    vector<array<double,3>> xprof(dim.nx-2, {0., 0., 0.}), yprof(dim.ny-2, {0., 0., 0.}), zprof(dim.nz-2, {0., 0., 0.});
    int num_profiles = 0; // Number of time iterations taken into account to compute time-averaged profiles.
    // Maximum number of time iterations depending on whether the simulation is in benchmark mode or production mode.
    int max_time_iter = benchmark ? bench_max_iter : static_cast<int>(max_t / dt);
    for (int time_iter = 0; time_iter < max_time_iter; ++time_iter) {
        if (benchmark && time_iter == bench_ini_iter)  {
            cout << "Now running " << bench_max_iter - bench_ini_iter
                 << " benchmark iterations." << endl;
            tie(start, clock_iter) = restartClock();
        }
        // The regression case has been registered at a resolution of N=102 after 150 iterations.
        // This test should be executed at every code change.
        if (N == 102 && time_iter == 150) {
            runRegression(computeEnergy(collide), time_iter, model);
        }
        // If this option is chosen in the config file, save the full domain in VTK files periodically.
        if (!benchmark && vtk_freq != 0 && time_iter % vtk_freq == 0 && time_iter > 0) {
            saveVtkFields(collide, time_iter);
        }
        // If this option is chosen in the config file, save the raw populations in text files periodically.
        if (!benchmark && data_freq != 0 && time_iter % data_freq == 0 && time_iter > 0) {
            saveFields(collide);
        }
        // If this option is chosen in the config file, output some statistics and write the center-line
        // velocity profiles to a text file.
        if (!benchmark && out_freq != 0 && time_iter % out_freq == 0 && time_iter > 0) {
            cout << "Saving profiles at iteration " << time_iter
                 << ", t = " << setprecision(4) << time_iter * dt << setprecision(3) << " [" << time_iter * dt / max_t * 100. << "%]" << endl;
            bool writeProfileToDisk = ( time_iter % (20*out_freq) == 0 );
            if (start_avg_profiles != 0. && time_iter * dt > start_avg_profiles) {
                ++num_profiles;
                saveProfiles(collide, ulb, xprof, yprof, zprof, num_profiles, writeProfileToDisk);
            }
            else {
                if (writeProfileToDisk) {
                    saveProfiles(collide, ulb);
                }
            }
            double energy = computeEnergy(collide) *dx*dx / (dt*dt);
            cout << "Average energy: " << setprecision(8) << energy << endl;
            energyfile << setw(10) << time_iter * dt << setw(16) << setprecision(8) << energy << "\n";
        }

        // Collision and streaming are not fused. Execute a collision step ...
        for_each(execution::par_unseq, lattice, lattice + dim.nelem, collide);
        // ... and thereafter a streaming step. The necessity for two for_each calls is motivated
        // by thread saftey (all threads are synchronized between subsequent calls to for_each).
        for_each(execution::par_unseq, lattice, lattice + dim.nelem, stream);
        ++clock_iter;
    }

    if (benchmark) {
        return printMlups(start, clock_iter, dim.nelem);
    }
    else {
        return 0.;
    }
}

// Runs a simulation of a flow in a lid-driven cavity using the AA-pattern.
// The function is templated to allow both an instantiation with the array-of-structure and
// the structure-of-array layout, and potentially an instantiation of unrolled or non-unrolled
// code.
template<class Even, class Odd>
double runCavityAA(bool benchmark, double Re, double ulb, int N, double yratio, double zratio, double max_t, LBModel model)
{
    // CellData is either a double (structure-of-array) or an array<double, 27> (array-of-structure).
    using CellData = typename Even::CellData;
    vector<double> ulid {0., 1., 0.}; // Velocity on top lid in dimensionless units.

    int nx = N;
    int ny = 2 + (int) round( (double)(N - 2) * yratio );
    int nz = 2 + (int) round( (double)(N - 2) * zratio );
    Dim dim {nx, ny, nz};

    auto[nu, omega, dx, dt] = lbParameters(ulb, ny - 2, Re);
    printParameters(benchmark, Re, omega, ulb, dim, max_t);

    // We use the "vector" as a convenient way to allocate the flag array on the heap.
    vector<CellData> lattice_vect(Even::sizeOfLattice(dim.nelem));
    CellData* lattice = &lattice_vect[0];

    vector<CellType> flag_vect(dim.nelem);
    CellType* flag = &flag_vect[0];

    // The lattice constants (discrete velocities, opposite indices, weghts) are mostly
    // used in the not-unrolled versions of the code. They must be allocated on the
    // heap so they can be shared with the device in case of a GPU execution.
    auto[c, opp, t] = d3q27_constants();
    // Instantiate two function objects for the for_each calls at even and at odd time
    // steps respectively. Note that collision and streaming are fused: only one of the
    // two is function objects is used at every time step.
    bool periodic = false;
    Even even{{lattice, flag, &c[0], &opp[0], &t[0], omega, dim, model, periodic}};
    Odd odd{{lattice, flag, &c[0], &opp[0], &t[0], omega, dim, model, periodic}};

    // Initialize the populations.
    for_each(lattice, lattice + dim.nelem, [&even](CellData& f0) { even.iniLattice(f0); });

    // Set up the geometry of the cavity.
    iniCavity(even, ulb, ulid);
    // Reset the clock, to be used when a benchmark simulation is executed.
    auto[start, clock_iter] = restartClock();

    // The average energy, dependent on time, can be used to monitor convergence, or statistical
    // convergence, of the simulation.
    ofstream energyfile("energy.dat", ofstream::out);
    // Storage space for the time-averaged profiles, if such profiles are computed.
    vector<array<double,3>> xprof(dim.nx-2, {0., 0., 0.}), yprof(dim.ny-2, {0., 0., 0.}), zprof(dim.nz-2, {0., 0., 0.});
    int num_profiles = 0; // Number of time iterations taken into account to compute time-averaged profiles.
    // Maximum number of time iterations depending on whether the simulation is in benchmark mode or production mode.
    int max_time_iter = benchmark ? bench_max_iter : static_cast<int>(max_t / dt);
    for (int time_iter = 0; time_iter < max_time_iter; ++time_iter) {
        if (benchmark && time_iter == bench_ini_iter)  {
            cout << "Now running " << bench_max_iter - bench_ini_iter
                 << " benchmark iterations." << endl;
            tie(start, clock_iter) = restartClock();
        }
        // The regression case has been registered at a resolution of N=102 after 150 iterations.
        // This test should be executed at every code change.
        if (N == 102 && time_iter == 150) {
            runRegression(computeEnergy(even), time_iter, model);
        }
        // If this option is chosen in the config file, save the full domain in VTK files periodically.
        if (!benchmark && vtk_freq != 0 && time_iter % vtk_freq == 0 && time_iter > 0) {
            saveVtkFields(even, time_iter);
        }
        // If this option is chosen in the config file, save the raw populations in text files periodically.
        if (!benchmark && data_freq != 0 && time_iter % data_freq == 0 && time_iter > 0) {
            saveFields(even);
        }
        // If this option is chosen in the config file, output some statistics and write the center-line
        // velocity profiles to a text file.
        if (!benchmark && out_freq != 0 && time_iter % out_freq == 0 && time_iter > 0) {
            cout << "Saving profiles at iteration " << time_iter
                 << ", t = " << setprecision(4) << time_iter * dt << setprecision(3) << " [" << time_iter * dt / max_t * 100. << "%]" << endl;
            bool writeProfileToDisk = ( time_iter % (20*out_freq) == 0 );
            if (start_avg_profiles != 0. && time_iter * dt > start_avg_profiles) {
                ++num_profiles;
                saveProfiles(even, ulb, xprof, yprof, zprof, num_profiles, writeProfileToDisk);
            }
            else {
                if (writeProfileToDisk) {
                    saveProfiles(even, ulb);
                }
            }
            double energy = computeEnergy(even) *dx*dx / (dt*dt);
            cout << "Average energy: " << setprecision(8) << energy << endl;
            energyfile << setw(10) << time_iter * dt << setw(16) << setprecision(8) << energy << "\n";
        }

        // Collision and streaming are fused. Depending on the parity of the current time step,
        // execute one function object or the other one.
        if (time_iter % 2 == 0) {
            for_each(execution::par_unseq, lattice, lattice + dim.nelem, even);
        }
        else {
            for_each(execution::par_unseq, lattice, lattice + dim.nelem, odd);
        }
        ++clock_iter;
    }

    if (benchmark) {
        return printMlups(start, clock_iter, dim.nelem);
    }
    else {
        return 0.;
    }
}

int main()
{
    try {
        set<string> double_param { "Re", "ulb", "max_t", "start_avg_profiles", "yratio", "zratio", "omega_bulk" };
        set<string> int_param { "N", "out_freq", "vtk_freq", "data_freq", "bench_ini_iter", "bench_max_iter" };
        set<string> bool_param { "benchmark", "unrolled", "adjust_bulk_viscosity" };
        set<string> string_param { "lbModel", "structure" };
        // Initialize the map with default parameters that do not need to be specified in the config file.
        map<string, std::any> param {
            { "yratio", 1.0 },
            { "zratio", 1.0 },
            { "adjust_bulk_viscosity", true },
            { "omega_bulk", 0.0 },
        };
        // Get all parameters from the configuration file (or in some cases, default values).
        param = parse_configfile(double_param, int_param, bool_param, string_param, param);

        try {
            string structureStr = any_cast<string>(param.at("structure"));
            param["structureStr"] = structureStr;
            param["structure"] = stringToDataStructure().at(structureStr);
        }
        catch(out_of_range const& e) {
            throw invalid_argument("Unknown data structure: " + any_cast<string>(param["structure"]));
        }
        try {
            string modelStr = any_cast<string>(param.at("lbModel"));
            param["lbModelStr"] = modelStr;
            param["lbModel"] = stringToLBModel().at(modelStr);
        }
        catch(out_of_range const& e) {
            throw invalid_argument("Unknown LB model: " + any_cast<string>(param["lbModel"]));
        }

        bool benchmark = any_cast<bool>(param["benchmark"]);
        double Re = any_cast<double>(param["Re"]);
        double ulb = any_cast<double>(param["ulb"]);
        int N = any_cast<int>(param["N"]);
        double yratio = any_cast<double>(param["yratio"]);
        double zratio = any_cast<double>(param["zratio"]);
        double max_t = any_cast<double>(param["max_t"]);
        LBModel model = any_cast<LBModel>(param["lbModel"]);
        string model_str = any_cast<string>(param["lbModelStr"]);
        DataStructure data_structure = any_cast<DataStructure>(param["structure"]);
        string data_structure_str = any_cast<string>(param["structureStr"]);
        bool unrolled = any_cast<bool>(param["unrolled"]);
        double omega_bulk = any_cast<double>(param["omega_bulk"]);

        out_freq = any_cast<int>(param["out_freq"]);
        vtk_freq = any_cast<int>(param["vtk_freq"]);
        data_freq = any_cast<int>(param["data_freq"]);
        start_avg_profiles = any_cast<double>(param["start_avg_profiles"]);
        bench_ini_iter = any_cast<int>(param["bench_ini_iter"]);
        bench_max_iter = any_cast<int>(param["bench_max_iter"]);

        double tmp_yLength = (double)(N - 2) * yratio;
        if (tmp_yLength - floor(tmp_yLength) > 1.e-12) {
            cout << "WARNING: after application of a y-ratio of " << yratio
                 << ", the y-length is NOT an integer number." << endl;
            cout << "Remember that the LU x-length of the cavity is N - 2 = " << N - 2 << endl;
        }
        double tmp_zLength = (double)(N - 2) * zratio;
        if (tmp_zLength - floor(tmp_zLength) > 1.e-12) {
            cout << "WARNING: after application of a z-ratio of " << zratio
                 << ", the z-length is NOT an integer number." << endl;
            cout << "Remember that the LU x-length of the cavity is N - 2 = " << N - 2 << endl;
        }

        if (omega_bulk != 0.) {
            cout << "WARNING: You provided a bulk viscosity, but this application does not implement user-selected bulk viscosities." << endl;
        }

        cout << "LB model: " << model_str << endl;
        cout << "Implementation scheme: " << data_structure_str << endl;
        cout << "Unrolled loops: " << boolalpha << unrolled << endl;

        // Now follows the long list of options to instantiate the right code depending on
        // - Whether to use the two-population scheme, the swap scheme, or the AA-pattern
        // - Which of the nine collision models to use
        // - Whether to use the optimized (unrolled) or educational (not unrolled) code.
        if (data_structure == DataStructure::twopop_aos) {
            if (unrolled) {
                if (model == LBModel::bgk) {
                    runCavityTwoPop<twopop_aos_bgk_unrolled::LBM>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else if (model == LBModel::trt) {
                    runCavityTwoPop<twopop_aos_trt_unrolled::LBM>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else {
                    throw invalid_argument("Unrolled twopop_aos implemented for BGK and TRT only.");
                }
            }
            else {
                if (model == LBModel::bgk || model == LBModel::trt) {
                    runCavityTwoPop<twopop_aos::LBM>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else {
                    throw invalid_argument("Educational twopop_aos implemented for BGK and TRT only.");
                }
            }
        }
        else if (data_structure == DataStructure::twopop_soa) {
            if (unrolled) {
                if (model == LBModel::bgk) {
                    runCavityTwoPop<twopop_soa_bgk_unrolled::LBM>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else if (model == LBModel::trt) {
                    runCavityTwoPop<twopop_soa_trt_unrolled::LBM>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else if (model == LBModel::bgk_o4) {
                    runCavityTwoPop<twopop_soa_bgk_o4::LBM>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else if (model == LBModel::rm) {
                    runCavityTwoPop<twopop_soa_rm::LBM>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else if (model == LBModel::hm) {
                    runCavityTwoPop<twopop_soa_hm::LBM>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else if (model == LBModel::cm) {
                    runCavityTwoPop<twopop_soa_cm::LBM>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else if (model == LBModel::chm) {
                    runCavityTwoPop<twopop_soa_chm::LBM>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else if (model == LBModel::k) {
                    runCavityTwoPop<twopop_soa_k::LBM>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else if (model == LBModel::rr) {
                    runCavityTwoPop<twopop_soa_rr::LBM>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else {
                    throw invalid_argument("Unrolled twopop_soa not implemented for the selected model.");
                }
            }
            else { // Not unrolled
                if (model == LBModel::bgk || model == LBModel::trt) {
                    runCavityTwoPop<twopop_soa::LBM>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else {
                    throw invalid_argument("Educational twopop_soa implemented for BGK and TRT only.");
                }
            }
        }
        else if (data_structure == DataStructure::swap_aos) {
            if (unrolled) {
                if (model == LBModel::bgk) {
                    runCavitySwap<swap_aos_bgk_unrolled::Collide, swap_aos_bgk_unrolled::Stream>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else if (model == LBModel::trt) {
                    runCavitySwap<swap_aos_trt_unrolled::Collide, swap_aos_trt_unrolled::Stream>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else {
                    throw invalid_argument("Unrolled swap_aos implemented for BGK and TRT only.");
                }
            }
            else { // Not unrolled
                if (model == LBModel::bgk || model == LBModel::trt) {
                    runCavitySwap<swap_aos::Collide, swap_aos::Stream>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else {
                    throw invalid_argument("Educational swap_aos implemented for BGK and TRT only.");
                }
            }
        }
        else if (data_structure == DataStructure::swap_soa) {
            if (unrolled) {
                if (model == LBModel::bgk) {
                    runCavitySwap<swap_soa_bgk_unrolled::Collide, swap_soa_bgk_unrolled::Stream>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else if (model == LBModel::trt) {
                    runCavitySwap<swap_soa_trt_unrolled::Collide, swap_soa_trt_unrolled::Stream>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else {
                    throw invalid_argument("Unrolled swap_soa implemented for BGK and TRT only.");
                }
            }
            else { // Not unrolled
                if (model == LBModel::bgk || model == LBModel::trt) {
                    runCavitySwap<swap_soa::Collide, swap_soa::Stream>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else {
                    throw invalid_argument("Educational swap_soa implemented for BGK and TRT only.");
                }
            }
        }
        else if (data_structure == DataStructure::aa_aos) {
            if (unrolled) {
                if (model == LBModel::bgk) {
                    runCavityAA<aa_aos_bgk_unrolled::Even, aa_aos_bgk_unrolled::Odd>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else if (model == LBModel::trt) {
                    runCavityAA<aa_aos_trt_unrolled::Even, aa_aos_trt_unrolled::Odd>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else {
                    throw invalid_argument("Unrolled aa_aos implemented for BGK and TRT only.");
                }
            }
            else { // Not unrolled
                if (model == LBModel::bgk ||model == LBModel::trt) {
                    runCavityAA<aa_aos::Even, aa_aos::Odd>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else {
                    throw invalid_argument("Educational aa_aos implemented for BGK and TRT only.");
                }
            }
        }
        else if (data_structure == DataStructure::aa_soa) {
            if (unrolled) {
                if (model == LBModel::bgk) {
                    runCavityAA<aa_soa_bgk_unrolled::Even, aa_soa_bgk_unrolled::Odd>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else if (model == LBModel::trt) {
                    runCavityAA<aa_soa_trt_unrolled::Even, aa_soa_trt_unrolled::Odd>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else if (model == LBModel::bgk_o4) {
                    runCavityAA<aa_soa_bgk_o4::Even, aa_soa_bgk_o4::Odd>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else if (model == LBModel::rm) {
                    runCavityAA<aa_soa_rm::Even, aa_soa_rm::Odd>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else if (model == LBModel::hm) {
                    runCavityAA<aa_soa_hm::Even, aa_soa_hm::Odd>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else if (model == LBModel::cm) {
                    runCavityAA<aa_soa_cm::Even, aa_soa_cm::Odd>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else if (model == LBModel::chm) {
                    runCavityAA<aa_soa_chm::Even, aa_soa_chm::Odd>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else if (model == LBModel::k) {
                    runCavityAA<aa_soa_k::Even, aa_soa_k::Odd>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else if (model == LBModel::rr) {
                    runCavityAA<aa_soa_rr::Even, aa_soa_rr::Odd>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else {
                    throw invalid_argument("Unrolled aa_soa not implemented for the selected model.");
                }
            }
            else { // Not unrolled
                if (model == LBModel::bgk || model == LBModel::trt) {
                    runCavityAA<aa_soa::Even, aa_soa::Odd>(benchmark, Re, ulb, N, yratio, zratio, max_t, model);
                }
                else {
                    throw invalid_argument("Educational aa_soa implemented for BGK and TRT only.");
                }
            }
        }
        else if (data_structure == DataStructure::all) {
            std::vector<double> twopop_aos_mlups, twopop_soa_mlups, swap_aos_mlups, swap_soa_mlups, aa_aos_mlups, aa_soa_mlups;
            benchmark = true; // "all" data structures triggers a special benchmark mode.
            
            if (unrolled) {
                if (model == LBModel::bgk) {
                    cout << "Running all benchmarks in bgk-unrolled mode." << endl;
                    for (int i = 0; i < 10; ++i) {
                        cout << "Running twopop_aos attempt " << i << endl;
                        twopop_aos_mlups.push_back(
                            runCavityTwoPop<twopop_aos_bgk_unrolled::LBM>(benchmark, Re, ulb, N, yratio, zratio, max_t, model) );
                        cout << "Running twopop_soa attempt " << i << endl;
                        twopop_soa_mlups.push_back(
                            runCavityTwoPop<twopop_soa_bgk_unrolled::LBM>(benchmark, Re, ulb, N, yratio, zratio, max_t, model) );
                        cout << "Running swap_aos attempt " << i << endl;
                        swap_aos_mlups.push_back(
                            runCavitySwap<swap_aos_bgk_unrolled::Collide, swap_aos_bgk_unrolled::Stream>(benchmark, Re, ulb, N, yratio, zratio, max_t, model) );
                        cout << "Running swap_soa attempt " << i << endl;
                        swap_soa_mlups.push_back(
                            runCavitySwap<swap_soa_bgk_unrolled::Collide, swap_soa_bgk_unrolled::Stream>(benchmark, Re, ulb, N, yratio, zratio, max_t, model) );
                        cout << "Running aa_aos attempt " << i << endl;
                        aa_aos_mlups.push_back(
                            runCavityAA<aa_aos_bgk_unrolled::Even, aa_aos_bgk_unrolled::Odd>(benchmark, Re, ulb, N, yratio, zratio, max_t, model) );
                        cout << "Running aa_soa attempt " << i << endl;
                        aa_soa_mlups.push_back(
                            runCavityAA<aa_soa_bgk_unrolled::Even, aa_soa_bgk_unrolled::Odd>(benchmark, Re, ulb, N, yratio, zratio, max_t, model) );
                    }
                }
                else if (model == LBModel::trt) {
                    cout << "Running all benchmarks in trt-unrolled mode." << endl;
                    for (int i = 0; i < 10; ++i) {
                        cout << "Running twopop_aos attempt " << i << endl;
                        twopop_aos_mlups.push_back(
                            runCavityTwoPop<twopop_aos_trt_unrolled::LBM>(benchmark, Re, ulb, N, yratio, zratio, max_t, model) );
                        cout << "Running twopop_soa attempt " << i << endl;
                        twopop_soa_mlups.push_back(
                            runCavityTwoPop<twopop_soa_trt_unrolled::LBM>(benchmark, Re, ulb, N, yratio, zratio, max_t, model) );
                        cout << "Running swap_aos attempt " << i << endl;
                        swap_aos_mlups.push_back(
                            runCavitySwap<swap_aos_trt_unrolled::Collide, swap_aos_trt_unrolled::Stream>(benchmark, Re, ulb, N, yratio, zratio, max_t, model) );
                        cout << "Running swap_soa attempt " << i << endl;
                        swap_soa_mlups.push_back(
                            runCavitySwap<swap_soa_trt_unrolled::Collide, swap_soa_trt_unrolled::Stream>(benchmark, Re, ulb, N, yratio, zratio, max_t, model) );
                        cout << "Running aa_aos attempt " << i << endl;
                        aa_aos_mlups.push_back(
                            runCavityAA<aa_aos_trt_unrolled::Even, aa_aos_trt_unrolled::Odd>(benchmark, Re, ulb, N, yratio, zratio, max_t, model) );
                        cout << "Running aa_soa attempt " << i << endl;
                        aa_soa_mlups.push_back(
                            runCavityAA<aa_soa_trt_unrolled::Even, aa_soa_trt_unrolled::Odd>(benchmark, Re, ulb, N, yratio, zratio, max_t, model) );
                    }
                }
            }
            else {
                cout << "Running all benchmarks in bgk mode." << endl;
                for (int i = 0; i < 10; ++i) {
                    cout << "Running twopop_aos attempt " << i << endl;
                    twopop_aos_mlups.push_back(
                        runCavityTwoPop<twopop_aos::LBM>(benchmark, Re, ulb, N, yratio, zratio, max_t, model) );
                    cout << "Running twopop_soa attempt " << i << endl;
                    twopop_soa_mlups.push_back(
                        runCavityTwoPop<twopop_soa::LBM>(benchmark, Re, ulb, N, yratio, zratio, max_t, model) );
                    cout << "Running swap_aos attempt " << i << endl;
                    swap_aos_mlups.push_back(
                        runCavitySwap<swap_aos::Collide, swap_aos::Stream>(benchmark, Re, ulb, N, yratio, zratio, max_t, model) );
                    cout << "Running swap_soa attempt " << i << endl;
                    swap_soa_mlups.push_back(
                        runCavitySwap<swap_soa::Collide, swap_soa::Stream>(benchmark, Re, ulb, N, yratio, zratio, max_t, model) );
                    cout << "Running aa_aos attempt " << i << endl;
                    aa_aos_mlups.push_back(
                        runCavityAA<aa_aos::Even, aa_aos::Odd>(benchmark, Re, ulb, N, yratio, zratio, max_t, model) );
                    cout << "Running aa_soa attempt " << i << endl;
                    aa_soa_mlups.push_back(
                        runCavityAA<aa_soa::Even, aa_soa::Odd>(benchmark, Re, ulb, N, yratio, zratio, max_t, model) );
                }
            }


            // Output the 10 performance measurements for each data structure, and the median value.
            cout << "Summary of performance measurements with Model " << model_str
                 << (unrolled? ", unrolled, " : ", educational, " ) << "N = " << N << endl;
            cout << setw(16) << "twopop_aos"
                 << setw(16) << "twopop_soa"
                 << setw(16) << "swap_aos"
                 << setw(16) << "swap_soa"
                 << setw(16) << "aa_aos"
                 << setw(16) << "aa_soa" << endl;
            for (int i = 0; i < 10; ++i) {
                cout << setw(16) << setprecision(5) << twopop_aos_mlups[i]
                     << setw(16) << setprecision(5) << twopop_soa_mlups[i]
                     << setw(16) << setprecision(5) << swap_aos_mlups[i]
                     << setw(16) << setprecision(5) << swap_soa_mlups[i]
                     << setw(16) << setprecision(5) << aa_aos_mlups[i]
                     << setw(16) << setprecision(5) << aa_soa_mlups[i]
                     << setw(16) << setprecision(5) << endl;
            }
            // Compute the median of a vector of 10 elements.
            auto median10 = [](vector<double> v) {
                assert( v.size() == 10 );
                nth_element(v.begin(), v.begin() + 5, v.end()); 
                nth_element(v.begin(), v.begin() + 4, v.end()); 
                return (double)(v[4] + v[5]) / 2.0; 
            };
            cout << "MEDIAN: " << endl;
            cout << setw(16) << setprecision(5) << median10(twopop_aos_mlups)
                 << setw(16) << setprecision(5) << median10(twopop_soa_mlups)
                 << setw(16) << setprecision(5) << median10(swap_aos_mlups)
                 << setw(16) << setprecision(5) << median10(swap_soa_mlups)
                 << setw(16) << setprecision(5) << median10(aa_aos_mlups)
                 << setw(16) << setprecision(5) << median10(aa_soa_mlups)
                 << setw(16) << setprecision(5) << endl;
        }
        else {
            assert( false );
        }
    }
    catch(runtime_error const& e) {
        cout << "Error: Regression test failed." << endl;
        return 2;
    }
    catch(exception const& e) {
        cout << "Error: invalid use of program." << endl;
        cout << e.what() << endl;
        return 1;
    }
    return 0;
}
