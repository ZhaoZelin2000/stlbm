// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#pragma once

#include "lbm.h"
#include <vector>
#include <array>
#include <tuple>
#include <iostream>

namespace twopop_soa_rm {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 2 * 19 * nelem; }

    CellData* lattice;
    CellType* flag;
    int* parity;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega1;
    Dim dim;
    LBModel model;
    bool periodic = false;
    double omegaBulk = 0.;
    double omega2 = omega1;
    double omega3 = omega1;
    double omega4 = omega1;

    auto i_to_xyz (int i) const {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) const {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    double& fin (int i, int k) {
        return lattice[*parity * dim.npop + k * dim.nelem + i];
    }
    
    double& fout (int i, int k) {
        return lattice[(1 - *parity) * dim.npop + k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 19; ++k) {
            fin(i, k) = t[k];
        }
    };

    auto iniLattice (double& f0, double rho, std::array<double, 3> const& u) {
        auto i = &f0 - lattice;
        double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        for (int k = 0; k < 19; ++k) {
            double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
            f(i, k) = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        }
    };

    // Required for the nonregression based on computeEnergy() 
    auto macro (double const& f0) {
        auto i = &f0 - lattice;
        double X_M1 = fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6);
        double X_P1 = fin(i,10) + fin(i,13) + fin(i,14) + fin(i,15) + fin(i,16);
        double X_0  = fin(i, 9) + fin(i, 1) + fin(i, 2) + fin(i, 7) + fin(i, 8) + fin(i,11) + fin(i,12) + fin(i,17) + fin(i,18);

        double Y_M1 = fin(i, 1) + fin(i, 3) + fin(i, 7) + fin(i, 8) + fin(i,14);
        double Y_P1 = fin(i, 4) + fin(i,11) + fin(i,13) + fin(i,17) + fin(i,18);

        double Z_M1 = fin(i, 2) + fin(i, 5) + fin(i, 7) + fin(i,16) + fin(i,18);
        double Z_P1 = fin(i, 6) + fin(i, 8) + fin(i,12) + fin(i,15) + fin(i,17);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return std::make_pair(rho, u);
    }
 
    // The function below has only an educational purpose since its optimized version 
    // computeRMopt() is preferred to achieve better performance
    auto computeRM(double& f0, double const& rho) {

        auto i = &f0 - lattice;
        std::array<double, 19> RM;
        std::fill(RM.begin(), RM.end(), 0.);

        for (int k = 0; k < 19; ++k) {
            // Order 2
            RM[M200] += fin(i,k)*c[k][0]*c[k][0];
            RM[M020] += fin(i,k)*c[k][1]*c[k][1];
            RM[M002] += fin(i,k)*c[k][2]*c[k][2];
            RM[M110] += fin(i,k)*c[k][0]*c[k][1];
            RM[M101] += fin(i,k)*c[k][0]*c[k][2];
            RM[M011] += fin(i,k)*c[k][1]*c[k][2];
            // Order 3
            RM[M210] += fin(i,k)*c[k][0]*c[k][0]*c[k][1];
            RM[M201] += fin(i,k)*c[k][0]*c[k][0]*c[k][2];
            RM[M021] += fin(i,k)*c[k][1]*c[k][1]*c[k][2];
            RM[M120] += fin(i,k)*c[k][0]*c[k][1]*c[k][1];
            RM[M102] += fin(i,k)*c[k][0]*c[k][2]*c[k][2];
            RM[M012] += fin(i,k)*c[k][1]*c[k][2]*c[k][2];
            // Order 4
            RM[M220] += fin(i,k)*c[k][0]*c[k][0]*c[k][1]*c[k][1];
            RM[M202] += fin(i,k)*c[k][0]*c[k][0]*c[k][2]*c[k][2];
            RM[M022] += fin(i,k)*c[k][1]*c[k][1]*c[k][2]*c[k][2];
        }

        double invRho = 1./rho;
        for (int k = 0; k < 19; ++k) {
            RM[k] *= invRho;
        }

        return RM;
    }

    auto computeRMopt(double& f0, double const& rho) {

        auto i = &f0 - lattice;
        std::array<double, 19> RM;

        double invRho = 1./rho;
         // Order 4
        RM[M220] = invRho * (fin(i, 3) + fin(i, 4) + fin(i,13) + fin(i,14));
        RM[M202] = invRho * (fin(i, 5) + fin(i, 6) + fin(i,15) + fin(i,16));
        RM[M022] = invRho * (fin(i, 7) + fin(i, 8) + fin(i,17) + fin(i,18));
         // Order 2
        RM[M200] = invRho * (fin(i, 0) + fin(i,10)) + RM[M220] + RM[M202];
        RM[M020] = invRho * (fin(i, 1) + fin(i,11)) + RM[M220] + RM[M022];
        RM[M002] = invRho * (fin(i, 2) + fin(i,12)) + RM[M202] + RM[M022];
        RM[M110] = RM[M220] - 2.*invRho * (fin(i, 4) + fin(i,14));
        RM[M101] = RM[M202] - 2.*invRho * (fin(i, 6) + fin(i,16));
        RM[M011] = RM[M022] - 2.*invRho * (fin(i, 8) + fin(i,18));
         // Order 3
        RM[M210] = RM[M220] - 2.*invRho * (fin(i, 3) + fin(i,14));
        RM[M201] = RM[M202] - 2.*invRho * (fin(i, 5) + fin(i,16));
        RM[M021] = RM[M022] - 2.*invRho * (fin(i, 7) + fin(i,18));
        RM[M120] = RM[M220] - 2.*invRho * (fin(i, 3) + fin(i, 4));
        RM[M102] = RM[M202] - 2.*invRho * (fin(i, 5) + fin(i, 6));
        RM[M012] = RM[M022] - 2.*invRho * (fin(i, 7) + fin(i, 8));

        return RM;
    }

    // Further optimization through merge with the computation of macros
    auto computeRMopt2(double& f0) {

        auto i = &f0 - lattice;
        std::array<double, 19> RM;
        std::fill(RM.begin(), RM.end(), 0.);

        double X_M1 = fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6);
        double X_P1 = fin(i,10) + fin(i,13) + fin(i,14) + fin(i,15) + fin(i,16);
        double X_0  = fin(i, 9) + fin(i, 1) + fin(i, 2) + fin(i, 7) + fin(i, 8) + fin(i,11) + fin(i,12) + fin(i,17) + fin(i,18);

        double Y_M1 = fin(i, 1) + fin(i, 3) + fin(i, 7) + fin(i, 8) + fin(i,14);
        double Y_P1 = fin(i, 4) + fin(i,11) + fin(i,13) + fin(i,17) + fin(i,18);

        double Z_M1 = fin(i, 2) + fin(i, 5) + fin(i, 7) + fin(i,16) + fin(i,18);
        double Z_P1 = fin(i, 6) + fin(i, 8) + fin(i,12) + fin(i,15) + fin(i,17);

        // Order 0
        RM[M000] = X_M1 + X_P1 + X_0;
        double invRho = 1. / RM[M000];
        double two_invRho= 2.*invRho;
        // Order 1
        RM[M100] = invRho * (X_P1 - X_M1);
        RM[M010] = invRho * (Y_P1 - Y_M1); 
        RM[M001] = invRho * (Z_P1 - Z_M1);
        // Order 2
        RM[M200] = invRho * (X_M1 + X_P1);
        RM[M020] = invRho * (Y_M1 + Y_P1);
        RM[M002] = invRho * (Z_M1 + Z_P1);
        RM[M110] = invRho * ( fin(i, 3) - fin(i, 4) + fin(i,13) - fin(i,14));
        RM[M101] = invRho * ( fin(i, 5) - fin(i, 6) + fin(i,15) - fin(i,16));
        RM[M011] = invRho * ( fin(i, 7) - fin(i, 8) + fin(i,17) - fin(i,18));
        // Order 3
        RM[M210] = RM[M110] - two_invRho * (fin(i, 3) - fin(i, 4));
        RM[M201] = RM[M101] - two_invRho * (fin(i, 5) - fin(i, 6));
        RM[M021] = RM[M011] - two_invRho * (fin(i, 7) - fin(i, 8));
        RM[M120] = RM[M110] - two_invRho * (fin(i, 3) - fin(i,14));
        RM[M102] = RM[M101] - two_invRho * (fin(i, 5) - fin(i,16));
        RM[M012] = RM[M011] - two_invRho * (fin(i, 7) - fin(i,18));
        // Order 4
        RM[M220] = RM[M110] + two_invRho * (fin(i, 4) + fin(i,14));
        RM[M202] = RM[M101] + two_invRho * (fin(i, 6) + fin(i,16));
        RM[M022] = RM[M011] + two_invRho * (fin(i, 8) + fin(i,18));

        return RM;
    }

    auto computeRMeq(std::array<double, 3> const& u) {

        std::array<double, 19> RMeq;
        double cs2 = 1./3.;
        // Order 2
        RMeq[M200] = u[0] * u[0] + cs2;
        RMeq[M020] = u[1] * u[1] + cs2;
        RMeq[M002] = u[2] * u[2] + cs2;
        RMeq[M110] = u[0] * u[1];
        RMeq[M101] = u[0] * u[2];
        RMeq[M011] = u[1] * u[2];
        // Order 3
        RMeq[M210] = RMeq[M200] * u[1];
        RMeq[M201] = RMeq[M200] * u[2];
        RMeq[M021] = RMeq[M020] * u[2];
        RMeq[M120] = RMeq[M020] * u[0];
        RMeq[M102] = RMeq[M002] * u[0];
        RMeq[M012] = RMeq[M002] * u[1];
        // Order 4
        RMeq[M220] = RMeq[M200] * RMeq[M020];
        RMeq[M202] = RMeq[M200] * RMeq[M002];
        RMeq[M022] = RMeq[M020] * RMeq[M002];

        return RMeq;
    }

    auto collideAndStreamRM(int i, int iX, int iY, int iZ, double rho, std::array<double, 3> const& u, std::array<double, 19> const& RM, std::array<double, 19> const& RMeq) {

        // Post-collision moments.
        std::array<double, 19> RMcoll;

        // Order 2
        if (omegaBulk == 0.) { // Don't adjust bulk viscosity
            RMcoll[M200] = (1. - omega1) * RM[M200] + omega1 * RMeq[M200];
            RMcoll[M020] = (1. - omega1) * RM[M020] + omega1 * RMeq[M020];
            RMcoll[M002] = (1. - omega1) * RM[M002] + omega1 * RMeq[M002];
        }
        else { // Adjust bulk viscosity
            const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
            const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
            RMcoll[M200] = RM[M200] - omegaPlus  * (RM[M200]-RMeq[M200]) - omegaMinus * (RM[M020]-RMeq[M020]) - omegaMinus * (RM[M002]-RMeq[M002]) ;
            RMcoll[M020] = RM[M020] - omegaMinus * (RM[M200]-RMeq[M200]) - omegaPlus  * (RM[M020]-RMeq[M020]) - omegaMinus * (RM[M002]-RMeq[M002]) ;
            RMcoll[M002] = RM[M002] - omegaMinus * (RM[M200]-RMeq[M200]) - omegaMinus * (RM[M020]-RMeq[M020]) - omegaPlus  * (RM[M002]-RMeq[M002]) ;
        }
        
        RMcoll[M110] = (1. - omega2) * RM[M110] + omega2 * RMeq[M110];
        RMcoll[M101] = (1. - omega2) * RM[M101] + omega2 * RMeq[M101];
        RMcoll[M011] = (1. - omega2) * RM[M011] + omega2 * RMeq[M011];

        // Order 3
        RMcoll[M210] = (1. - omega3) * RM[M210] + omega3 * RMeq[M210];
        RMcoll[M201] = (1. - omega3) * RM[M201] + omega3 * RMeq[M201];
        RMcoll[M021] = (1. - omega3) * RM[M021] + omega3 * RMeq[M021];
        RMcoll[M120] = (1. - omega3) * RM[M120] + omega3 * RMeq[M120];
        RMcoll[M102] = (1. - omega3) * RM[M102] + omega3 * RMeq[M102];
        RMcoll[M012] = (1. - omega3) * RM[M012] + omega3 * RMeq[M012];
        
        // Order 4
        RMcoll[M220] = (1. - omega4) * RM[M220] + omega4 * RMeq[M220];
        RMcoll[M202] = (1. - omega4) * RM[M202] + omega4 * RMeq[M202];
        RMcoll[M022] = (1. - omega4) * RM[M022] + omega4 * RMeq[M022];

        // Optimization based on symmetries between populations and their opposite counterpart
        double pop_out_09 = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022]);
        
        double pop_out_opp_00 = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202]);
        double pop_out_00     =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]) + pop_out_opp_00;

        double pop_out_opp_01 = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022]);
        double pop_out_01     =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]) + pop_out_opp_01;

        double pop_out_opp_02 = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022]);
        double pop_out_02     =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]) + pop_out_opp_02;

        double pop_out_opp_03 = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] + RMcoll[M220]);
        double pop_out_04     =  0.5*rho * (-RMcoll[M110]                - RMcoll[M120]) + pop_out_opp_03;
        double pop_out_opp_04 =  0.5*rho * (-RMcoll[M110] - RMcoll[M210])                + pop_out_opp_03;
        double pop_out_03     =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]) + pop_out_opp_03;

        double pop_out_opp_05 = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] + RMcoll[M202]);
        double pop_out_06     =  0.5*rho * (-RMcoll[M101]                - RMcoll[M102]) + pop_out_opp_05;
        double pop_out_opp_06 =  0.5*rho * (-RMcoll[M101] - RMcoll[M201])                + pop_out_opp_05;
        double pop_out_05     =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]) + pop_out_opp_05;

        double pop_out_opp_07 = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] + RMcoll[M022]);
        double pop_out_08     =  0.5*rho * (-RMcoll[M011]                - RMcoll[M012]) + pop_out_opp_07;
        double pop_out_opp_08 =  0.5*rho * (-RMcoll[M011] - RMcoll[M021])                + pop_out_opp_07;
        double pop_out_07     =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]) + pop_out_opp_07;


        int XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        int YY = iY;
        int ZZ = iZ;
        size_t nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 10) = pop_out_00 + f(nb, 0);
        }
        else {
            fout(nb, 0) = pop_out_00;
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx : iX + 1;
        YY = iY;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 0) = pop_out_opp_00 + f(nb,10);
        }
        else {
            fout(nb,10) = pop_out_opp_00;
        }

        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 11) = pop_out_01 + f(nb, 1);
        }
        else {
            fout(nb, 1) = pop_out_01;
        }

        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 1) = pop_out_opp_01 + f(nb,11);
        }
        else {
            fout(nb,11) = pop_out_opp_01;
        }

        XX = iX;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 12) = pop_out_02 + f(nb, 2);
        }
        else {
            fout(nb, 2) = pop_out_02;
        }

        XX = iX;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 2) = pop_out_opp_02 + f(nb,12);
        }
        else {
            fout(nb,12) = pop_out_opp_02;
        }

        XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 13) = pop_out_03 + f(nb, 3);
        }
        else {
            fout(nb, 3) = pop_out_03;
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx : iX + 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 3) = pop_out_opp_03 + f(nb,13);
        }
        else {
            fout(nb,13) = pop_out_opp_03;
        }

        XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 14) = pop_out_04 + f(nb, 4);
        }
        else {
            fout(nb, 4) = pop_out_04;
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx : iX + 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 4) = pop_out_opp_04 + f(nb,14);
        }
        else {
            fout(nb,14) = pop_out_opp_04;
        }

        XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 15) = pop_out_05 + f(nb, 5);
        }
        else {
            fout(nb, 5) = pop_out_05;
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx : iX + 1;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 5) = pop_out_opp_05 + f(nb,15);
        }
        else {
            fout(nb,15) = pop_out_opp_05;
        }

        XX = periodic ? (iX - 1 + dim.nx) % dim.nx : iX - 1;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 16) = pop_out_06 + f(nb, 6);
        }
        else {
            fout(nb, 6) = pop_out_06;
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx : iX + 1;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 6) = pop_out_opp_06 + f(nb,16);
        }
        else {
            fout(nb,16) = pop_out_opp_06;
        }

        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 17) = pop_out_07 + f(nb, 7);
        }
        else {
            fout(nb, 7) = pop_out_07;
        }

        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 7) = pop_out_opp_07 + f(nb,17);
        }
        else {
            fout(nb,17) = pop_out_opp_07;
        }

        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny : iY - 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz : iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 18) = pop_out_08 + f(nb, 8);
        }
        else {
            fout(nb, 8) = pop_out_08;
        }

        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny : iY + 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz : iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 8) = pop_out_opp_08 + f(nb,18);
        }
        else {
            fout(nb,18) = pop_out_opp_08;
        }


        fout(i, 9) =  pop_out_09;
    }


    void iterateRM(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            auto[iX, iY, iZ] = i_to_xyz(i);
            // auto[rho, u] = macro(f0);
            // auto RM = computeRM(f0, rho);
            // auto RM = computeRMopt(f0, rho);
            auto RM = computeRMopt2(f0);
            double rho = RM[M000];
            std::array<double, 3> u = {RM[M100], RM[M010], RM[M001]};  
            auto RMeq = computeRMeq(u);
            collideAndStreamRM(i, iX, iY, iZ, rho, u, RM, RMeq);
        }
    }

    void operator() (double& f0) {
        iterateRM(f0);
    }
};

} // namespace twopop_soa_rm

