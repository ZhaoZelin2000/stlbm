// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#pragma once

#include "lbm_q27.h"
#include <vector>
#include <array>
#include <tuple>
#include <iostream>

namespace twopop_soa_rm {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 2 * 27 * nelem; }

    CellData* lattice;
    CellType* flag;
    int* parity;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega1;
    Dim dim;
    LBModel model;
    bool periodic = false;
    double omegaBulk = 0.;
    double omega2 = omega1;
    double omega3 = omega1;
    double omega4 = omega1;
    double omega5 = omega1;
    double omega6 = omega1;
    double omega7 = omega1;
    double omega8 = omega1;

    auto i_to_xyz (int i) const {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) const {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    double& fin (int i, int k) {
        return lattice[*parity * dim.npop + k * dim.nelem + i];
    }
    
    double& fout (int i, int k) {
        return lattice[(1 - *parity) * dim.npop + k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 27; ++k) {
            fin(i, k) = t[k];
        }
    };

    auto iniLattice (double& f0, double rho, std::array<double, 3> const& u) {
        auto i = &f0 - lattice;
        double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        for (int k = 0; k < 27; ++k) {
            double ck_u = c[k][0] * u[0] + c[k][1] * u[1] + c[k][2] * u[2];
            f(i, k) = rho * t[k] * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - usqr);
        }
    };

    // Required for the nonregression based on computeEnergy() 
    auto macro (double const& f0) {
        auto i = &f0 - lattice;
        double X_M1 = fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12);
        double X_P1 = fin(i,14) + fin(i,17) + fin(i,18) + fin(i,19) + fin(i,20) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26);
        double X_0  = fin(i, 1) + fin(i, 2) + fin(i, 7) + fin(i, 8) + fin(i,13) + fin(i,15) + fin(i,16) + fin(i,21) + fin(i,22);

        double Y_M1 = fin(i, 1) + fin(i, 3) + fin(i, 7) + fin(i, 8) + fin(i, 9) + fin(i,10) + fin(i,18) + fin(i,25) + fin(i,26);
        double Y_P1 = fin(i,15) + fin(i,17) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) + fin(i, 4) + fin(i,11) + fin(i,12);

        double Z_M1 = fin(i, 2) + fin(i, 5) + fin(i, 7) + fin(i, 9) + fin(i,11) + fin(i,20) + fin(i,22) + fin(i,24) + fin(i,26);
        double Z_P1 = fin(i,16) + fin(i,19) + fin(i,21) + fin(i,23) + fin(i,25) + fin(i, 6) + fin(i, 8) + fin(i,10) + fin(i,12);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return std::make_pair(rho, u);
    }
 
    // Naive way to compute RM and macros
    auto computeRM(double& f0) {

        auto i = &f0 - lattice;
        std::array<double, 27> RM;
        std::fill(RM.begin(), RM.end(), 0.);

        for (int k = 0; k<27; ++k) {
            // Order 0
            RM[M000] += fin(i,k);
            // Order 1
            RM[M100] += c[k][0] * fin(i,k);
            RM[M010] += c[k][1] * fin(i,k);
            RM[M001] += c[k][2] * fin(i,k);
            // Order 2
            RM[M200] += c[k][0] * c[k][0] * fin(i,k);
            RM[M020] += c[k][1] * c[k][1] * fin(i,k);
            RM[M002] += c[k][2] * c[k][2] * fin(i,k);
            RM[M110] += c[k][0] * c[k][1] * fin(i,k);
            RM[M101] += c[k][0] * c[k][2] * fin(i,k);
            RM[M011] += c[k][1] * c[k][2] * fin(i,k);
            // Order 3
            RM[M210] += c[k][0] * c[k][0] * c[k][1] * fin(i,k);
            RM[M201] += c[k][0] * c[k][0] * c[k][2] * fin(i,k);
            RM[M021] += c[k][1] * c[k][1] * c[k][2] * fin(i,k);
            RM[M120] += c[k][0] * c[k][1] * c[k][1] * fin(i,k);
            RM[M102] += c[k][0] * c[k][2] * c[k][2] * fin(i,k);
            RM[M012] += c[k][1] * c[k][2] * c[k][2] * fin(i,k);
            RM[M111] += c[k][0] * c[k][1] * c[k][2] * fin(i,k);
            // Order 4
            RM[M220] += c[k][0] * c[k][0] * c[k][1] * c[k][1] * fin(i,k);
            RM[M202] += c[k][0] * c[k][0] * c[k][2] * c[k][2] * fin(i,k);
            RM[M022] += c[k][1] * c[k][1] * c[k][2] * c[k][2] * fin(i,k);
            RM[M211] += c[k][0] * c[k][0] * c[k][1] * c[k][2] * fin(i,k);
            RM[M121] += c[k][0] * c[k][1] * c[k][1] * c[k][2] * fin(i,k);
            RM[M112] += c[k][0] * c[k][1] * c[k][2] * c[k][2] * fin(i,k);
            // Order 5
            RM[M221] += c[k][0] * c[k][0] * c[k][1] * c[k][1] * c[k][2] * fin(i,k);
            RM[M212] += c[k][0] * c[k][0] * c[k][1] * c[k][2] * c[k][2] * fin(i,k);
            RM[M122] += c[k][0] * c[k][1] * c[k][1] * c[k][2] * c[k][2] * fin(i,k);
            // Order 6
            RM[M222] += c[k][0] * c[k][0] * c[k][1] * c[k][1] * c[k][2] * c[k][2] * fin(i,k);
        }

        double invRho = 1. / RM[M000];
        for (int k = 1; k<27; ++k) {
            RM[k] *= invRho;
        }
        return RM;
    }

    // First optimization (loop unrolling)
    auto computeRMopt(double& f0) {

        auto i = &f0 - lattice;
        std::array<double, 27> RM;
        std::fill(RM.begin(), RM.end(), 0.);
        // Order 0
        RM[M000] =  fin(i, 0) + fin(i, 1) + fin(i, 2) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6) + fin(i, 7) + fin(i, 8) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12) + fin(i,13) + fin(i,14) + fin(i,15) + fin(i,16) + fin(i,17) + fin(i,18) + fin(i,19) + fin(i,20) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26);
        double invRho = 1./RM[M000];
        // Order 1
        RM[M100] = invRho * (- fin(i, 0) - fin(i, 3) - fin(i, 4) - fin(i, 5) - fin(i, 6) - fin(i, 9) - fin(i,10) - fin(i,11) - fin(i,12) + fin(i,14) + fin(i,17) + fin(i,18) + fin(i,19) + fin(i,20) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        RM[M010] = invRho * (- fin(i, 1) - fin(i, 3) + fin(i, 4) - fin(i, 7) - fin(i, 8) - fin(i, 9) - fin(i,10) + fin(i,11) + fin(i,12) + fin(i,15) + fin(i,17) - fin(i,18) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) - fin(i,25) - fin(i,26));
        RM[M001] = invRho * (- fin(i, 2) - fin(i, 5) + fin(i, 6) - fin(i, 7) + fin(i, 8) - fin(i, 9) + fin(i,10) - fin(i,11) + fin(i,12) + fin(i,16) + fin(i,19) - fin(i,20) + fin(i,21) - fin(i,22) + fin(i,23) - fin(i,24) + fin(i,25) - fin(i,26));
        // Order 2
        RM[M200] = invRho * (  fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12) + fin(i,14) + fin(i,17) + fin(i,18) + fin(i,19) + fin(i,20) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        RM[M020] = invRho * (  fin(i, 1) + fin(i, 3) + fin(i, 4) + fin(i, 7) + fin(i, 8) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12) + fin(i,15) + fin(i,17) + fin(i,18) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        RM[M002] = invRho * (  fin(i, 2) + fin(i, 5) + fin(i, 6) + fin(i, 7) + fin(i, 8) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12) + fin(i,16) + fin(i,19) + fin(i,20) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        RM[M110] = invRho * (  fin(i, 3) - fin(i, 4) + fin(i, 9) + fin(i,10) - fin(i,11) - fin(i,12) + fin(i,17) - fin(i,18) + fin(i,23) + fin(i,24) - fin(i,25) - fin(i,26));
        RM[M101] = invRho * (  fin(i, 5) - fin(i, 6) + fin(i, 9) - fin(i,10) + fin(i,11) - fin(i,12) + fin(i,19) - fin(i,20) + fin(i,23) - fin(i,24) + fin(i,25) - fin(i,26));
        RM[M011] = invRho * (  fin(i, 7) - fin(i, 8) + fin(i, 9) - fin(i,10) - fin(i,11) + fin(i,12) + fin(i,21) - fin(i,22) + fin(i,23) - fin(i,24) - fin(i,25) + fin(i,26));
        // Order 3
        RM[M210] = invRho * (- fin(i, 3) + fin(i, 4) - fin(i, 9) - fin(i,10) + fin(i,11) + fin(i,12) + fin(i,17) - fin(i,18) + fin(i,23) + fin(i,24) - fin(i,25) - fin(i,26));
        RM[M201] = invRho * (- fin(i, 5) + fin(i, 6) - fin(i, 9) + fin(i,10) - fin(i,11) + fin(i,12) + fin(i,19) - fin(i,20) + fin(i,23) - fin(i,24) + fin(i,25) - fin(i,26));
        RM[M021] = invRho * (- fin(i, 7) + fin(i, 8) - fin(i, 9) + fin(i,10) - fin(i,11) + fin(i,12) + fin(i,21) - fin(i,22) + fin(i,23) - fin(i,24) + fin(i,25) - fin(i,26));
        RM[M120] = invRho * (- fin(i, 3) - fin(i, 4) - fin(i, 9) - fin(i,10) - fin(i,11) - fin(i,12) + fin(i,17) + fin(i,18) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        RM[M102] = invRho * (- fin(i, 5) - fin(i, 6) - fin(i, 9) - fin(i,10) - fin(i,11) - fin(i,12) + fin(i,19) + fin(i,20) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        RM[M012] = invRho * (- fin(i, 7) - fin(i, 8) - fin(i, 9) - fin(i,10) + fin(i,11) + fin(i,12) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) - fin(i,25) - fin(i,26));
        RM[M111] = invRho * (- fin(i, 9) + fin(i,10) + fin(i,11) - fin(i,12) + fin(i,23) - fin(i,24) - fin(i,25) + fin(i,26));
        // Order 4
        RM[M220] = invRho * (  fin(i, 3) + fin(i, 4) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12) + fin(i,17) + fin(i,18) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        RM[M202] = invRho * (  fin(i, 5) + fin(i, 6) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12) + fin(i,19) + fin(i,20) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        RM[M022] = invRho * (  fin(i, 7) + fin(i, 8) + fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        RM[M211] = invRho * (  fin(i, 9) - fin(i,10) - fin(i,11) + fin(i,12) + fin(i,23) - fin(i,24) - fin(i,25) + fin(i,26));
        RM[M121] = invRho * (  fin(i, 9) - fin(i,10) + fin(i,11) - fin(i,12) + fin(i,23) - fin(i,24) + fin(i,25) - fin(i,26));
        RM[M112] = invRho * (  fin(i, 9) + fin(i,10) - fin(i,11) - fin(i,12) + fin(i,23) + fin(i,24) - fin(i,25) - fin(i,26));
        // Order 5
        RM[M221] = invRho * (- fin(i, 9) + fin(i,10) - fin(i,11) + fin(i,12) + fin(i,23) - fin(i,24) + fin(i,25) - fin(i,26));
        RM[M212] = invRho * (- fin(i, 9) - fin(i,10) + fin(i,11) + fin(i,12) + fin(i,23) + fin(i,24) - fin(i,25) - fin(i,26));
        RM[M122] = invRho * (- fin(i, 9) - fin(i,10) - fin(i,11) - fin(i,12) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));
        // Order 6
        RM[M222] = invRho * (  fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12) + fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26));

        return RM;
    }

    // Most optimized version (loop unrolling + redundant terms)
    // Further pre-computing terms is counter-productive
    auto computeRMopt2(double& f0) {

        auto i = &f0 - lattice;
        std::array<double, 27> RM;
        std::fill(RM.begin(), RM.end(), 0.);

        double A1 = fin(i, 9) + fin(i,10) + fin(i,11) + fin(i,12);
        double A2 = fin(i,23) + fin(i,24) + fin(i,25) + fin(i,26);
        double A3 = fin(i, 9) + fin(i,10) - fin(i,11) - fin(i,12);
        double A4 = fin(i,23) + fin(i,24) - fin(i,25) - fin(i,26);
        double A5 = fin(i, 9) - fin(i,10) + fin(i,11) - fin(i,12);
        double A6 = fin(i,23) - fin(i,24) + fin(i,25) - fin(i,26);
        double A7 = fin(i, 9) - fin(i,10) - fin(i,11) + fin(i,12);
        double A8 = fin(i,23) - fin(i,24) - fin(i,25) + fin(i,26);

        double X_M1 = fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6) + A1;
        double X_P1 = fin(i,14) + fin(i,17) + fin(i,18) + fin(i,19) + fin(i,20) + A2;
        double X_0  = fin(i, 1) + fin(i, 2) + fin(i, 7) + fin(i, 8) + fin(i,13) + fin(i,15) + fin(i,16) + fin(i,21) + fin(i,22);

        double Y_M1 = fin(i, 1) + fin(i, 3) + fin(i, 7) + fin(i, 8) + fin(i, 9) + fin(i,10) + fin(i,18) + fin(i,25) + fin(i,26);
        double Y_P1 = fin(i,15) + fin(i,17) + fin(i,21) + fin(i,22) + fin(i,23) + fin(i,24) + fin(i, 4) + fin(i,11) + fin(i,12);

        double Z_M1 = fin(i, 2) + fin(i, 5) + fin(i, 7) + fin(i, 9) + fin(i,11) + fin(i,20) + fin(i,22) + fin(i,24) + fin(i,26);
        double Z_P1 = fin(i,16) + fin(i,19) + fin(i,21) + fin(i,23) + fin(i,25) + fin(i, 6) + fin(i, 8) + fin(i,10) + fin(i,12);

        // Order 0
        RM[M000] = X_M1 + X_P1 + X_0;
        double invRho = 1. / RM[M000];

        // Order 6
        RM[M222] = invRho * (  A1 + A2);
        // Order 5
        RM[M221] = invRho * (- A5 + A6);
        RM[M212] = invRho * (- A3 + A4);
        RM[M122] = invRho * (- A1 + A2);
        // Order 4
        RM[M220] = invRho * (  fin(i, 3) + fin(i, 4) + fin(i,17) + fin(i,18)) + RM[M222];
        RM[M202] = invRho * (  fin(i, 5) + fin(i, 6) + fin(i,19) + fin(i,20)) + RM[M222];
        RM[M022] = invRho * (  fin(i, 7) + fin(i, 8) + fin(i,21) + fin(i,22)) + RM[M222];
        RM[M211] = invRho * (  A7 + A8);
        RM[M121] = invRho * (  A5 + A6);
        RM[M112] = invRho * (  A3 + A4);
        // Order 3
        RM[M210] = invRho * (- fin(i, 3) + fin(i, 4) + fin(i,17) - fin(i,18)) + RM[M212];
        RM[M201] = invRho * (- fin(i, 5) + fin(i, 6) + fin(i,19) - fin(i,20)) + RM[M221];
        RM[M021] = invRho * (- fin(i, 7) + fin(i, 8) + fin(i,21) - fin(i,22)) + RM[M221];
        RM[M120] = invRho * (- fin(i, 3) - fin(i, 4) + fin(i,17) + fin(i,18)) + RM[M122];
        RM[M102] = invRho * (- fin(i, 5) - fin(i, 6) + fin(i,19) + fin(i,20)) + RM[M122];
        RM[M012] = invRho * (- fin(i, 7) - fin(i, 8) + fin(i,21) + fin(i,22)) + RM[M212];
        RM[M111] = invRho * (- A7 + A8);
        // Order 2
        RM[M200] = invRho * (X_P1 + X_M1);
        RM[M020] = invRho * (Y_P1 + Y_M1); 
        RM[M002] = invRho * (Z_P1 + Z_M1);
        RM[M110] = invRho * (  fin(i, 3) - fin(i, 4) + fin(i,17) - fin(i,18)) + RM[M112];
        RM[M101] = invRho * (  fin(i, 5) - fin(i, 6) + fin(i,19) - fin(i,20)) + RM[M121];
        RM[M011] = invRho * (  fin(i, 7) - fin(i, 8) + fin(i,21) - fin(i,22)) + RM[M211];
        // Order 1
        RM[M100] = invRho * (X_P1 - X_M1);
        RM[M010] = invRho * (Y_P1 - Y_M1); 
        RM[M001] = invRho * (Z_P1 - Z_M1);

        return RM;
    }

    auto computeRMeq(std::array<double, 3> const& u) {

        std::array<double, 27> RMeq;
        std::fill(RMeq.begin(), RMeq.end(), 0.);
        double cs2 = 1./3.;

        // Order 2
        RMeq[M200] = u[0] * u[0] + cs2;
        RMeq[M020] = u[1] * u[1] + cs2;
        RMeq[M002] = u[2] * u[2] + cs2;
        RMeq[M110] = u[0] * u[1];
        RMeq[M101] = u[0] * u[2];
        RMeq[M011] = u[1] * u[2];
        // Order 3
        RMeq[M210] = RMeq[M200] * u[1];
        RMeq[M201] = RMeq[M200] * u[2];
        RMeq[M021] = RMeq[M020] * u[2];
        RMeq[M120] = RMeq[M020] * u[0];
        RMeq[M102] = RMeq[M002] * u[0];
        RMeq[M012] = RMeq[M002] * u[1];
        RMeq[M111] = RMeq[M110] * u[2];
        // Order 4
        RMeq[M220] = RMeq[M200] * RMeq[M020];
        RMeq[M202] = RMeq[M200] * RMeq[M002];
        RMeq[M022] = RMeq[M020] * RMeq[M002];
        RMeq[M211] = RMeq[M200] * RMeq[M011];
        RMeq[M121] = RMeq[M020] * RMeq[M101];
        RMeq[M112] = RMeq[M002] * RMeq[M110];
        // Order 5
        RMeq[M221] = RMeq[M220] * u[2];
        RMeq[M212] = RMeq[M202] * u[1];
        RMeq[M122] = RMeq[M022] * u[0];
        // Order 6
        RMeq[M222] = RMeq[M220] * RMeq[M002];

        return RMeq;
    }

    auto collideAndStreamRM(int i, int iX, int iY, int iZ, double rho, std::array<double, 3> const& u, std::array<double, 27> const& RM, std::array<double, 27> const& RMeq)
    {    
        // Post-collision moments.
        std::array<double, 27> RMcoll;

        // Order 2
        if (omegaBulk == 0.) { // Don't adjust bulk viscosity 
            RMcoll[M200] = (1. - omega1) * RM[M200] + omega1 * RMeq[M200];
            RMcoll[M020] = (1. - omega1) * RM[M020] + omega1 * RMeq[M020];
            RMcoll[M002] = (1. - omega1) * RM[M002] + omega1 * RMeq[M002];
        }
        else { // Adjust bulk viscosity
            const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
            const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
            RMcoll[M200] = RM[M200] - omegaPlus  * (RM[M200]-RMeq[M200]) - omegaMinus * (RM[M020]-RMeq[M020]) - omegaMinus * (RM[M002]-RMeq[M002]) ;
            RMcoll[M020] = RM[M020] - omegaMinus * (RM[M200]-RMeq[M200]) - omegaPlus  * (RM[M020]-RMeq[M020]) - omegaMinus * (RM[M002]-RMeq[M002]) ;
            RMcoll[M002] = RM[M002] - omegaMinus * (RM[M200]-RMeq[M200]) - omegaMinus * (RM[M020]-RMeq[M020]) - omegaPlus  * (RM[M002]-RMeq[M002]) ;
        }

        RMcoll[M110] = (1. - omega2) * RM[M110] + omega2 * RMeq[M110] ;
        RMcoll[M101] = (1. - omega2) * RM[M101] + omega2 * RMeq[M101] ;
        RMcoll[M011] = (1. - omega2) * RM[M011] + omega2 * RMeq[M011] ;

        // Order 3
        RMcoll[M210] = (1. - omega3) * RM[M210] + omega3 * RMeq[M210] ;
        RMcoll[M201] = (1. - omega3) * RM[M201] + omega3 * RMeq[M201] ;
        RMcoll[M021] = (1. - omega3) * RM[M021] + omega3 * RMeq[M021] ;
        RMcoll[M120] = (1. - omega3) * RM[M120] + omega3 * RMeq[M120] ;
        RMcoll[M102] = (1. - omega3) * RM[M102] + omega3 * RMeq[M102] ;
        RMcoll[M012] = (1. - omega3) * RM[M012] + omega3 * RMeq[M012] ;
        
        RMcoll[M111] = (1. - omega4) * RM[M111] + omega4 * RMeq[M111] ;

        // Order 4
        RMcoll[M220] = (1. - omega5) * RM[M220] + omega5 * RMeq[M220] ;
        RMcoll[M202] = (1. - omega5) * RM[M202] + omega5 * RMeq[M202] ;
        RMcoll[M022] = (1. - omega5) * RM[M022] + omega5 * RMeq[M022] ;

        RMcoll[M211] = (1. - omega6) * RM[M211] + omega6 * RMeq[M211] ;
        RMcoll[M121] = (1. - omega6) * RM[M121] + omega6 * RMeq[M121] ;
        RMcoll[M112] = (1. - omega6) * RM[M112] + omega6 * RMeq[M112] ;

        // Order 5
        RMcoll[M221] = (1. - omega7) * RM[M221] + omega7 * RMeq[M221] ;
        RMcoll[M212] = (1. - omega7) * RM[M212] + omega7 * RMeq[M212] ;
        RMcoll[M122] = (1. - omega7) * RM[M122] + omega7 * RMeq[M122] ;

        // Order 6
        RMcoll[M222] = (1. - omega8) * RM[M222] + omega8 * RMeq[M222] ;

        // Optimization based on symmetries between populations and their opposite counterpart
        std::array<double, 27> foutRM;

        foutRM[F000] = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022] - RMcoll[M222]);
        
        foutRM[FP00] = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202] + RMcoll[M122] + RMcoll[M222]);
        foutRM[FM00] =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]                               - RMcoll[M122])+ foutRM[FP00];

        foutRM[F0P0] = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022] + RMcoll[M212] + RMcoll[M222]);
        foutRM[F0M0] =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]                               - RMcoll[M212])+ foutRM[F0P0];

        foutRM[F00P] = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022] + RMcoll[M221] + RMcoll[M222]);
        foutRM[F00M] =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]                               - RMcoll[M221])+ foutRM[F00P];

        foutRM[FPP0] = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] - RMcoll[M112] + RMcoll[M220] - RMcoll[M212] - RMcoll[M122] - RMcoll[M222]);
        foutRM[FMP0] =  0.5*rho * (-RMcoll[M110]                - RMcoll[M120] + RMcoll[M112]                               + RMcoll[M122])+ foutRM[FPP0];
        foutRM[FPM0] =  0.5*rho * (-RMcoll[M110] - RMcoll[M210]                + RMcoll[M112]                + RMcoll[M212]               )+ foutRM[FPP0];
        foutRM[FMM0] =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]                               + RMcoll[M212] + RMcoll[M122])+ foutRM[FPP0];

        foutRM[FP0P] = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] - RMcoll[M121] + RMcoll[M202] - RMcoll[M221] - RMcoll[M122] - RMcoll[M222]);
        foutRM[FM0P] =  0.5*rho * (-RMcoll[M101]                - RMcoll[M102] + RMcoll[M121]                               + RMcoll[M122])+ foutRM[FP0P];
        foutRM[FP0M] =  0.5*rho * (-RMcoll[M101] - RMcoll[M201]                + RMcoll[M121]                + RMcoll[M221]               )+ foutRM[FP0P];
        foutRM[FM0M] =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]                               + RMcoll[M221] + RMcoll[M122])+ foutRM[FP0P];

        foutRM[F0PP] = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] - RMcoll[M211] + RMcoll[M022] - RMcoll[M221] - RMcoll[M212] - RMcoll[M222]);
        foutRM[F0MP] =  0.5*rho * (-RMcoll[M011]                - RMcoll[M012] + RMcoll[M211]                               + RMcoll[M212])+ foutRM[F0PP];
        foutRM[F0PM] =  0.5*rho * (-RMcoll[M011] - RMcoll[M021]                + RMcoll[M211]                + RMcoll[M221]               )+ foutRM[F0PP];
        foutRM[F0MM] =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]                               + RMcoll[M221] + RMcoll[M212])+ foutRM[F0PP];

        foutRM[FPPP] = 0.125*rho * ( RMcoll[M111] + RMcoll[M211] + RMcoll[M121] + RMcoll[M112] + RMcoll[M221] + RMcoll[M212] + RMcoll[M122] + RMcoll[M222]);
        foutRM[FMPP] =  0.25*rho * (-RMcoll[M111]                - RMcoll[M121] - RMcoll[M112]                               - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FPMP] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211]                - RMcoll[M112]                - RMcoll[M212]               )+ foutRM[FPPP];
        foutRM[FPPM] =  0.25*rho * (-RMcoll[M111] - RMcoll[M211] - RMcoll[M121]                - RMcoll[M221]                              )+ foutRM[FPPP];
        foutRM[FMMP] =  0.25*rho * (              - RMcoll[M211] - RMcoll[M121]                               - RMcoll[M212] - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FMPM] =  0.25*rho * (              - RMcoll[M211]                - RMcoll[M112] - RMcoll[M221]                - RMcoll[M122])+ foutRM[FPPP];
        foutRM[FPMM] =  0.25*rho * (                             - RMcoll[M121] - RMcoll[M112] - RMcoll[M221] - RMcoll[M212]               )+ foutRM[FPPP];
        foutRM[FMMM] =  0.25*rho * (-RMcoll[M111]                                              - RMcoll[M221] - RMcoll[M212] - RMcoll[M122])+ foutRM[FPPP];

        // Unrolled streaming step
        int XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        int YY = iY;
        int ZZ = iZ;
        size_t nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 0]) = foutRM[0] + f(nb, 0);
        }
        else {
            fout(nb, 0) = foutRM[0];
        }
        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 1]) = foutRM[1] + f(nb, 1);
        }
        else {
            fout(nb, 1) = foutRM[1];
        }
        XX = iX;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 2]) = foutRM[2] + f(nb, 2);
        }
        else {
            fout(nb, 2) = foutRM[2];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 3]) = foutRM[3] + f(nb, 3);
        }
        else {
            fout(nb, 3) = foutRM[3];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 4]) = foutRM[4] + f(nb, 4);
        }
        else {
            fout(nb, 4) = foutRM[4];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 5]) = foutRM[5] + f(nb, 5);
        }
        else {
            fout(nb, 5) = foutRM[5];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 6]) = foutRM[6] + f(nb, 6);
        }
        else {
            fout(nb, 6) = foutRM[6];
        }
        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 7]) = foutRM[7] + f(nb, 7);
        }
        else {
            fout(nb, 7) = foutRM[7];
        }
        XX = iX;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 8]) = foutRM[8] + f(nb, 8);
        }
        else {
            fout(nb, 8) = foutRM[8];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[ 9]) = foutRM[9] + f(nb, 9);
        }
        else {
            fout(nb, 9) = foutRM[9];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[10]) = foutRM[10] + f(nb,10);
        }
        else {
            fout(nb,10) = foutRM[10];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[11]) = foutRM[11] + f(nb,11);
        }
        else {
            fout(nb,11) = foutRM[11];
        }
        XX = periodic ? (iX - 1 + dim.nx) % dim.nx: iX - 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[12]) = foutRM[12] + f(nb,12);
        }
        else {
            fout(nb,12) = foutRM[12];
        }


        fout(i,13) = foutRM[13];


        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = iY;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[14]) = foutRM[14] + f(nb,14);
        }
        else {
            fout(nb,14) = foutRM[14];
        }
        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[15]) = foutRM[15] + f(nb,15);
        }
        else {
            fout(nb,15) = foutRM[15];
        }
        XX = iX;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[16]) = foutRM[16] + f(nb,16);
        }
        else {
            fout(nb,16) = foutRM[16];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[17]) = foutRM[17] + f(nb,17);
        }
        else {
            fout(nb,17) = foutRM[17];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[18]) = foutRM[18] + f(nb,18);
        }
        else {
            fout(nb,18) = foutRM[18];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = iY;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[19]) = foutRM[19] + f(nb,19);
        }
        else {
            fout(nb,19) = foutRM[19];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = iY;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[20]) = foutRM[20] + f(nb,20);
        }
        else {
            fout(nb,20) = foutRM[20];
        }
        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[21]) = foutRM[21] + f(nb,21);
        }
        else {
            fout(nb,21) = foutRM[21];
        }
        XX = iX;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[22]) = foutRM[22] + f(nb,22);
        }
        else {
            fout(nb,22) = foutRM[22];
        }

        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[23]) = foutRM[23] + f(nb,23);
        }
        else {
            fout(nb,23) = foutRM[23];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = periodic ? (iY + 1 + dim.ny) % dim.ny: iY + 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[24]) = foutRM[24] + f(nb,24);
        }
        else {
            fout(nb,24) = foutRM[24];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = periodic ? (iZ + 1 + dim.nz) % dim.nz: iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[25]) = foutRM[25] + f(nb,25);
        }
        else {
            fout(nb,25) = foutRM[25];
        }
        XX = periodic ? (iX + 1 + dim.nx) % dim.nx: iX + 1;
        YY = periodic ? (iY - 1 + dim.ny) % dim.ny: iY - 1;
        ZZ = periodic ? (iZ - 1 + dim.nz) % dim.nz: iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i,opp[26]) = foutRM[26] + f(nb,26);
        }
        else {
            fout(nb,26) = foutRM[26];
        }
    }

    void iterateRM(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            auto[iX, iY, iZ] = i_to_xyz(i);
            // auto RM = computeRM(f0);
            // auto RM = computeRMopt(f0);
            auto RM = computeRMopt2(f0);
            double rho = RM[M000];
            std::array<double, 3> u = {RM[M100], RM[M010], RM[M001]};  
            auto RMeq = computeRMeq(u);
            collideAndStreamRM(i, iX, iY, iZ, rho, u, RM, RMeq);
        }
    }

    void operator() (double& f0) {
        iterateRM(f0);
    }
};

} // namespace twopop_soa_rm

